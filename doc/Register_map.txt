
0: Cntrl	
	0:1 Clock Freq.
		0: 2MHz
		1: 4MHz
		2: 10MHz
		3: 25MHz		
	2:2 Continuous Clock
		0: Disabled
		1: Enabled
	3:5 Output Mode
		0: XY2-100 16bit		
		1: XY2-100 18bit
		2: Dual AD5683
	6:6	Input Mode
		0: Data Port
		1: AXI		
	31:31 Reset core (
		0: NOP
		1: Reset (core is kept in reset while bit is set)
	
1: Data0
	0:17 Data
	
2: Data1
	0:17 Data
	
3: Update
	0:0
		0: NOP
		1: Send AXI-Data when Input-Mode is AXI (is reset by logic)
	
4: Status
	 