
#JM1 BANK66
set_property PACKAGE_PIN E9 [get_ports IOA[11]];#[get_ports B66_L18_P];#p36 - IOA11 - IOAdiff[0]_P
set_property PACKAGE_PIN D9 [get_ports IOA[10]];#[get_ports B66_L18_N];#p38 - IOA10 - IOAdiff[0]_N

set_property PACKAGE_PIN G8 [get_ports IOB[11]];#[get_ports B66_L16_P];#p40 - IOB11 - IOBdiff[0]_P
set_property PACKAGE_PIN F7 [get_ports IOB[10]];#[get_ports B66_L16_N];#p42 - IOB10 -IOBdiff[0]_N

set_property PACKAGE_PIN E8 [get_ports IOA[9]];#[get_ports B66_L17_N];#p46 - IOA9 - IOAdiff[1]_N 
set_property PACKAGE_PIN F8 [get_ports IOA[8]];#[get_ports B66_L17_P];#p48 - IOA8 - IOAdiff[1]_P
set_property PACKAGE_PIN G6 [get_ports IOB[9]];#[get_ports B66_L15_P];#p50 - IOB9 - IOBdiff[1]_P
set_property PACKAGE_PIN F6 [get_ports IOB[8]];#[get_ports B66_L15_N];#p52 - IOB8 - IOBdiff[1]_N

set_property PACKAGE_PIN F2 [get_ports IOA[7]];#[get_ports B66_L3_P];#p56 - IOA7 - IOAdiff[2]_P
set_property PACKAGE_PIN E2 [get_ports IOA[6]];#[get_ports B66_L3_N];#p58 - IOA6 - IOAdiff[2]_N
set_property PACKAGE_PIN E4 [get_ports IOB[7]];#[get_ports B66_L5_P];#p60 - IOB7 - IOBdiff[2]_P
set_property PACKAGE_PIN E3 [get_ports IOB[6]];#[get_ports B66_L5_N];#p62 - IOB6 - IOBdiff[2]_N

#set_property PACKAGE_PIN D6 [get_ports SPI0_sck_o];#[get_ports B66_L13_N];#p66 -CLK
#set_property PACKAGE_PIN D7 [get_ports SPI0_mosi_o];#[get_ports B66_L13_P];#p68 -CLK
set_property PACKAGE_PIN C9 [get_ports IOA[5]];#[get_ports B66_L24_P];#p70 - IOA5 - IOAdiff[3]_P
set_property PACKAGE_PIN B9 [get_ports IOA[4]];#[get_ports B66_L24_N];#p72 - IOA4 - IOAdiff[3]_N

set_property PACKAGE_PIN A8 [get_ports IOB[5]];#[get_ports B66_L23_N];#p76 - IOB5 - IOBdiff[3]_N
set_property PACKAGE_PIN A9 [get_ports IOB[4]];#[get_ports B66_L23_P];#p78 - IOB4 - IOBdiff[3]_P
set_property PACKAGE_PIN A7 [get_ports IOB[3]];#[get_ports B66_L21_P];#p80 - IOB3
set_property PACKAGE_PIN A6 [get_ports IOA[3]];#[get_ports B66_L21_N];#p82 - IOA3

# set_property PACKAGE_PIN A5 [get_ports SPI1_miso_i];#[get_ports B66_L19_N];#p86
set_property PACKAGE_PIN B5 [get_ports IOA[2]];#[get_ports B66_L19_P];#p88 - IOA2

set_property PACKAGE_PIN A4 [get_ports IOB[0]];#[get_ports B66_L10_N];#p94 - IOB0 - IOBdiff[0]_N
set_property PACKAGE_PIN B4 [get_ports IOB[1]];#[get_ports B66_L10_P];#p96 - IOB1 - IOBdiff[0]_P
#set_property PACKAGE_PIN A3 [get_ports B66_L9_N];#p98
set_property PACKAGE_PIN B3 [get_ports IOA[0]];#p100 - IOA0

#set_property PACKAGE_PIN F5 [get_ports B66_L6_N];#p31
#set_property PACKAGE_PIN G5 [get_ports B66_L6_P];#p33
#set_property PACKAGE_PIN C8 [get_ports B66_L22_P];#p35
#set_property PACKAGE_PIN B8 [get_ports B66_L22_N];#p37

set_property PACKAGE_PIN G3 [get_ports IOA[13]];#[get_ports B66_L4_P];#p41
set_property PACKAGE_PIN F3 [get_ports IOA[1]];#[get_ports B66_L4_N];#p43
set_property PACKAGE_PIN B6 [get_ports IOA[12]];#[get_ports B66_L20_N];#p43
set_property PACKAGE_PIN C6 [get_ports IOB[16]];#[get_ports B66_L20_P];#p45
set_property PACKAGE_PIN B1 [get_ports IOB[15]];#[get_ports B66_L7_N];#p47
set_property PACKAGE_PIN C1 [get_ports IOB[14]];#[get_ports B66_L7_P];#p51

set_property PACKAGE_PIN D1 [get_ports IOB[13]];#[get_ports B66_L2_N];#p55
set_property PACKAGE_PIN E1 [get_ports IOB[12]];#[get_ports B66_L2_P];#p57
#set_property PACKAGE_PIN D5 [get_ports B66_L14_N];#p59 -CLK
#set_property PACKAGE_PIN E5 [get_ports B66_L14_P];#p61 -CLK

#set_property PACKAGE_PIN C4 [get_ports B66_L11_N];#p65 -CLK
#set_property PACKAGE_PIN D4 [get_ports B66_L11_P];#p67 -CLK
set_property PACKAGE_PIN G1 [get_ports IOB[2]];#[get_ports B66_L1_P];#p69
#set_property PACKAGE_PIN F1 [get_ports B66_L1_N];#p71

#set_property PACKAGE_PIN C2 [get_ports B66_L12_N];#p75 -CLK
#set_property PACKAGE_PIN C3 [get_ports B66_L12_P];#p77 -CLK

#set_property PACKAGE_PIN A2 [get_ports B66_L8_P];#p81
#set_property PACKAGE_PIN A1 [get_ports B66_L8_N];#p83


#JM2 BANK64
set_property PACKAGE_PIN AB8 [get_ports PB_P[0]];#[get_ports B64_L3_P];#p32
set_property PACKAGE_PIN AC8 [get_ports PB_N[0]];#[get_ports B64_L3_N];#p34
set_property PACKAGE_PIN AC9 [get_ports IOB[26]];#[get_ports B64_L1_P];#p36
#set_property PACKAGE_PIN AD9 [get_ports PA_P[0]];#[get_ports B64_L1_N];#p38

set_property PACKAGE_PIN AE9 [get_ports PB_P[1]];#[get_ports B64_L2_P];#p42
set_property PACKAGE_PIN AE8 [get_ports PB_N[1]];#[get_ports B64_L2_N];#p44
set_property PACKAGE_PIN AF7 [get_ports PB_P[3]];#[get_ports B64_L11_P];#p46 -CLK
set_property PACKAGE_PIN AF6 [get_ports PB_N[3]];#[get_ports B64_L11_N];#p48 -CLK

set_property PACKAGE_PIN AE5 [get_ports PB_P[2]];#[get_ports B64_L12_P];#p52 -CLK
set_property PACKAGE_PIN AF5 [get_ports PB_N[2]];#[get_ports B64_L12_N];#p54 -CLK
set_property PACKAGE_PIN AD5 [get_ports IOB[34]];#[get_ports B64_L13_P];#p56 -CLK
set_property PACKAGE_PIN AD4 [get_ports IOB[35]];#[get_ports B64_L13_N];#p58 -CLK

set_property PACKAGE_PIN AG9 [get_ports IOA[34]];#[get_ports B64_L7_P];#p62
set_property PACKAGE_PIN AH9 [get_ports IOA[35]];#[get_ports B64_L7_N];#p64
set_property PACKAGE_PIN AF8 [get_ports IOB[33]];#[get_ports B64_L8_P];#p66
set_property PACKAGE_PIN AG8 [get_ports IOA[32]];#[get_ports B64_L8_N];#p68

set_property PACKAGE_PIN AH8 [get_ports IOA[33]];#[get_ports B64_L9_P];#p72
set_property PACKAGE_PIN AH7 [get_ports IOB[32]];#[get_ports B64_L9_N];#p74
set_property PACKAGE_PIN AE7 [get_ports IOB[31]];#[get_ports B64_L4_N];#p76
set_property PACKAGE_PIN AD7 [get_ports IOA[31]];#[get_ports B64_L4_P];#p78

set_property PACKAGE_PIN AB7 [get_ports IOA[30]];#[get_ports B64_L5_P];#p82
set_property PACKAGE_PIN AC7 [get_ports IOB[30]];#[get_ports B64_L5_N];#p84
set_property PACKAGE_PIN AB6 [get_ports IOA[29]];#[get_ports B64_L6_P];#p86
set_property PACKAGE_PIN AC6 [get_ports IOA[28]];#[get_ports B64_L6_N];#p88

set_property PACKAGE_PIN AF1 [get_ports IOB[29]];#[get_ports B64_L24_P];#p92
#set_property PACKAGE_PIN AG1 [get_ports B64_L24_N];#p94
set_property PACKAGE_PIN AH2 [get_ports IOA[27]];#[get_ports B64_L23_P];#p96
set_property PACKAGE_PIN AH1 [get_ports IOB[27]];#[get_ports B64_L23_N];#p98
set_property PACKAGE_PIN AH6 [get_ports IOA[26]];#[get_ports B64_T1];#p100

set_property PACKAGE_PIN AB3 [get_ports IOB[25]];#[get_ports B64_L15_N];#p41
set_property PACKAGE_PIN AB4 [get_ports IOB[19]];#[get_ports B64_L15_P];#p43
set_property PACKAGE_PIN AB2 [get_ports IOA[24]];#[get_ports B64_L17_P];#p45
set_property PACKAGE_PIN AC2 [get_ports IOA[18]];#[get_ports B64_L17_N];#p47

set_property PACKAGE_PIN AC4 [get_ports IOA[19]];#[get_ports B64_L14_P];#p51 -CLK
set_property PACKAGE_PIN AC3 [get_ports IOA[25]];#[get_ports B64_L14_N];#p53 -CLK
set_property PACKAGE_PIN AB1 [get_ports IOB[21]];#[get_ports B64_L18_P];#p55
set_property PACKAGE_PIN AC1 [get_ports IOB[22]];#[get_ports B64_L18_N];#p57

set_property PACKAGE_PIN AD2 [get_ports IOA[22]];#[get_ports B64_L16_P];#p61
set_property PACKAGE_PIN AD1 [get_ports IOA[20]];#[get_ports B64_L16_N];#p63
set_property PACKAGE_PIN AE3 [get_ports IOA[21]];#[get_ports B64_L21_P];#p65
set_property PACKAGE_PIN AF3 [get_ports IOB[20]];#[get_ports B64_L21_N];#p67

set_property PACKAGE_PIN AE2 [get_ports IOB[23]];#[get_ports B64_L22_P];#p71
set_property PACKAGE_PIN AF2 [get_ports IOA[23]];#[get_ports B64_L22_N];#p73
#set_property PACKAGE_PIN AG6 [get_ports B64_L10_P];#p75
#set_property PACKAGE_PIN AG5 [get_ports B64_L10_N];#p77

#set_property PACKAGE_PIN AG4 [get_ports B64_L19_P];#p81
#set_property PACKAGE_PIN AH4 [get_ports B64_L19_N];#p83
#set_property PACKAGE_PIN AG3 [get_ports B64_L20_P];#p85
#set_property PACKAGE_PIN AH3 [get_ports B64_L20_N];#p87
#set_property PACKAGE_PIN AD6 [get_ports B64_T0];#p89

#JM2 BANK65 1,8V
#set_property PACKAGE_PIN P7 [get_ports B65_L16_P];#p14
#set_property PACKAGE_PIN P6 [get_ports B65_L16_N];#p16

set_property PACKAGE_PIN L1 [get_ports PA_P[0]];#[get_ports B65_L7_P];#p22
set_property PACKAGE_PIN K1 [get_ports PA_N[0]];#[get_ports B65_L7_N];#p24
set_property PACKAGE_PIN N7 [get_ports PA_P[1]];#[get_ports B65_L15_P];#p26
set_property PACKAGE_PIN N6 [get_ports PA_N[1]];#[get_ports B65_L15_N];#p28

set_property PACKAGE_PIN H9 [get_ports IOB[17]];#[get_ports B65_L24_P];#p11
set_property PACKAGE_PIN H8 [get_ports IOA[14]];#[get_ports B65_L24_N];#p13
set_property PACKAGE_PIN R8 [get_ports IOA[15]];#[get_ports B65_L4_P];#p15
set_property PACKAGE_PIN T8 [get_ports IOB[18]];#[get_ports B65_L4_N];#p17

set_property PACKAGE_PIN M6 [get_ports PA_P[3]];#[get_ports B65_L14_P];#p21 -CLK
set_property PACKAGE_PIN L5 [get_ports PA_N[3]];#[get_ports B65_L14_N];#p23 -CLK
set_property PACKAGE_PIN L3 [get_ports PA_P[2]];#[get_ports B65_L12_P];#p25 -CLK
set_property PACKAGE_PIN L2 [get_ports PA_N[2]];#[get_ports B65_L12_N];#p27 -CLK

#set_property PACKAGE_PIN L8 [get_ports B65_L18_N];#p31
#set_property PACKAGE_PIN M8 [get_ports B65_L18_P];#p33
#set_property PACKAGE_PIN J7 [get_ports B65_L21_P];#p35
#set_property PACKAGE_PIN H7 [get_ports B65_L21_N];#p37



#JM3 BANK65 1,8V
set_property PACKAGE_PIN U8 [get_ports tracedata_o[0]];#[get_ports B65_L3_P];#p37
set_property PACKAGE_PIN V8 [get_ports tracedata_o[1]];#[get_ports B65_L3_N];#p39
set_property PACKAGE_PIN N9 [get_ports tracedata_o[2]];#[get_ports B65_L17_P];#p41
set_property PACKAGE_PIN N8 [get_ports tracedata_o[3]];#[get_ports B65_L17_N];#p43

set_property PACKAGE_PIN K4 [get_ports tracedata_o[4]];#[get_ports B65_L11_P];#p57 -CLK
set_property PACKAGE_PIN K3 [get_ports tracedata_o[5]];#[get_ports B65_L11_N];#p59 -CLK

set_property PACKAGE_PIN W8 [get_ports tracedata_o[6]];#[get_ports B65_L1_P];#p38
set_property PACKAGE_PIN Y8 [get_ports tracedata_o[7]];#[get_ports B65_L1_N];#p40
set_property PACKAGE_PIN R7 [get_ports tracedata_o[8]];#[get_ports B65_L5_P];#p42
set_property PACKAGE_PIN T7 [get_ports tracedata_o[9]];#[get_ports B65_L5_N];#p44

set_property PACKAGE_PIN U9 [get_ports tracedata_o[10]];#[get_ports B65_L2_P];#p48
set_property PACKAGE_PIN V9 [get_ports tracedata_o[11]];#[get_ports B65_L2_N];#p50
set_property PACKAGE_PIN R6 [get_ports tracedata_o[12]];#[get_ports B65_L6_P];#p52
set_property PACKAGE_PIN T6 [get_ports tracedata_o[13]];#[get_ports B65_L6_N];#p54

set_property PACKAGE_PIN L7 [get_ports tracedata_o[14]];#[get_ports B65_L13_P];#p58 -CLK
set_property PACKAGE_PIN L6 [get_ports tracedata_o[15]];#[get_ports B65_L13_N];#p60 -CLK



# Set the bank voltage for IO Bank 64..66 to 1.8V by default.
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 64]];
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 65]];
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 66]];

