/******************************************************************************
*
* Copyright (C) 2019 Koenig & Bauer Coding GmbH.  All rights reserved.
*
******************************************************************************/
/*****************************************************************************/
/**
*
* @file kbcScanOut.h
*
* This header file contains identifiers and driver functions (or
* macros) that can be used to access the device.
*
* <pre>
* MODIFICATION HISTORY:
*
* Ver   Who  Date     Changes
* ----- ---- -------- -----------------------------------------------
* 1.00  mh2  12/04/19 First release of low level driver
*
* </pre>
*
******************************************************************************/
#ifndef SCANOUT_H
#define SCANOUT_H


/****************** Include Files ********************/
#include "xil_types.h"
#include "xstatus.h"
#include "xil_io.h"

/* Register offsets */
#define KBCSCANOUT_CR_OFFSET 	0x00000000U
#define KBCSCANOUT_DATA0_OFFSET 0x00000004U
#define KBCSCANOUT_DATA1_OFFSET 0x00000008U
#define KBCSCANOUT_UR_OFFSET 	0x0000000CU
#define KBCSCANOUT_SR_OFFSET 	0x00000010U

/* Control reg offsets*/
#define KBCSCANOUT_CLKFREQ_MASK	0x00000003U
#define KBCSCANOUT_CONTCLK_MASK 0x00000004U
#define KBCSCANOUT_OUTMODE_MASK	0x00000031U
#define KBCSCANOUT_INMODE_MASK	0x00000040U
#define KBCSCANOUT_RESET_MASK	0x80000000U

#define KBCSCANOUT_CLKFREQ_SHIFT	 0U
#define KBCSCANOUT_CONTCLK_SHIFT	 2U
#define KBCSCANOUT_OUTMODE_SHIFT	 3U
#define KBCSCANOUT_INMODE_SHIFT		 6U
#define KBCSCANOUT_RESET_SHIFT		31U

/* Update reg */
#define KBCSCANOUT_UPDATE_MASK	0x00000001U

#define KBCSCANOUT_UPDATE_SHIFT	 	 0U

/**************************** Type Definitions *****************************/

/**
 * This typedef contains configuration information for the device.
 */
typedef struct {
	u16 DeviceId;		/**< Unique ID of device */
	u32 BaseAddress;		/**< Register base address */
} KBCScanOut_Config;

typedef enum {ScanSclk2MHz, ScanSclk5MHz, ScanSclk10MHz, ScanSclk25MHz} KBCScanOut_SclkFreq ;

typedef enum {ScanXY2_100_16Bit, ScanXY2_100_18Bit, ScanAD5683} KBCScanOut_OutMode;

/**
 * The KBCScanOut driver instance data. The user is required to allocate a
 * variable of this type for every KBCScanOut device in the system. A pointer
 * to a variable of this type is then passed to the driver API functions.
 */
typedef struct {
	UINTPTR BaseAddress;	/* Device base address */
	u32 IsReady;			/* Device is initialized and ready */
} KBCScanOut;

/**
 *
 * Write a value to a AXI_SCANOUT register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the AXI_SCANOUTdevice.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void AXI_SCANOUT_mWriteReg(u32 BaseAddress, unsigned RegOffset, u32 Data)
 *
 */
#define KBCScanOut_mWriteReg(BaseAddress, RegOffset, Data) \
  	Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))

/**
 *
 * Read a value from a AXI_SCANOUT register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the AXI_SCANOUT device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	u32 AXI_SCANOUT_mReadReg(u32 BaseAddress, unsigned RegOffset)
 *
 */
#define KBCScanOut_mReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))

/************************** Function Prototypes ****************************/
/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the AXI_SCANOUT instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus KBCScanOut_Reg_SelfTest(void * baseaddr_p);


/* Intialization functions */
KBCScanOut_Config *KBCScanOut_LookupConfig(u16 DeviceId);
int KBCScanOut_CfgInitialize(KBCScanOut* pInst, KBCScanOut_Config* pConfig,
			UINTPTR EffectiveAddr);
int KBCScanOut_Initialize(KBCScanOut* pInst, u16 DeviceId);

/* Basic API functions */
void KBCScanOut_SetInputAXI(KBCScanOut *pInst, u32 enable);
void KBCScanOut_SetOutMode(KBCScanOut *pInst, KBCScanOut_OutMode freq);
void KBCScanOut_SetSclkContinuous(KBCScanOut *pInst, u32 enable);
void KBCScanOut_SetSclkFreq(KBCScanOut *pInst, KBCScanOut_SclkFreq freq);
void KBCScanOut_Update(KBCScanOut *pInst);
void KBCScanOut_WriteData(KBCScanOut *pInst, u32 Ch, u32 Data);

#endif // SCANOUT_H

