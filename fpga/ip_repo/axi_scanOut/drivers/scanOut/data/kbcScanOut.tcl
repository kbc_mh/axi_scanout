

proc generate {drv_handle} {
	xdefine_include_file $drv_handle "xparameters.h" "KBCScanOut" "NUM_INSTANCES" "DEVICE_ID"  "BASEADDR" "HIGHADDR"
	
	xdefine_config_file $drv_handle "kbcScanOut_g.c" "KBCScanOut" "DEVICE_ID" "BASEADDR"
}
