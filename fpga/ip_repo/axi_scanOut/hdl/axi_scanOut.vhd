library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity axi_scanOut_v1_0 is
	generic (
		-- Users to add parameters here
		
		-- User parameters ends
		-- Do not modify the parameters beyond this line
    
		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 4
	);
	port (
		-- Users to add ports here
    data0_i : in std_logic_vector(17 downto 0);
    data1_i : in std_logic_vector(17 downto 0);
    strb_i : in std_logic;
    ready_o : out std_logic;
    sclk_o : out std_logic;
    sync_o : out std_logic;
    sdata0_o : out std_logic;
    sdata1_o : out std_logic;
    debug_o : out std_logic_vector(15 downto 0);
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end axi_scanOut_v1_0;

architecture arch_imp of axi_scanOut_v1_0 is

	-- component declaration
	component axi_scanOut_v1_0_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 4
		);
		port (
		cntrlReg_o : out std_logic_vector(31 downto 0);
    data0_o : out std_logic_vector(31 downto 0);
    data1_o : out std_logic_vector(31 downto 0);
    update_o : out std_logic;
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component axi_scanOut_v1_0_S00_AXI;

 

  component dataCntrl is
    generic ( 
      G_DATA_WIDTH : natural := 24 );     
    Port (     
      clk_i : in STD_LOGIC;
      arstn_i : in STD_LOGIC;           
      cntrlReg_i : in std_logic_vector(31 downto 0);
      strb_i : in std_logic;
      data0_i : in std_logic_vector(17 downto 0);
      data1_i : in std_logic_vector(17 downto 0);
      axiStrb_i : in std_logic;
      axiData0_i : in std_logic_vector(31 downto 0); 
      axiData1_i : in std_logic_vector(31 downto 0);    
      clkDiv_o : out std_logic_vector(5 downto 0);  
      contClkEn_o : out std_logic;       
      spiMode_o : out std_logic_vector(2 downto 0);
      srst_o : out std_logic;
      strb_o : out std_logic;           
      data0_o : out std_logic_vector(23 downto 0);
      data1_o : out std_logic_vector(23 downto 0)
      );
  end component dataCntrl;
  
  component scanOutSpi is
    generic ( 
      G_SREG_WIDTH : natural := 24 );     
    port ( 
      clk_i : in STD_LOGIC;
      srst_i : in STD_LOGIC;           
      strb_i : in STD_LOGIC;
      contClkEn_i : in STD_LOGIC;
      mode_i : in STD_LOGIC_VECTOR(2 downto 0);
      clkDiv_i : in STD_LOGIC_VECTOR(5 downto 0);
      data0_i : in STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0);
      data1_i : in STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0);
      ready_o : out std_logic;
      sclk_o : out std_logic;
      sync_o : out std_logic;
      sdata0_o : out std_logic;
      sdata1_o : out std_logic 
    );
  end component scanOutSpi;
  
  
  
  constant C_SREG_WIDTH : natural := 24;
  
  signal s_sRst : std_logic;

  signal s_cntrlReg : std_logic_vector(31 downto 0);
  signal s_spiMode : std_logic_vector(2 downto 0);
  signal s_clkDiv : std_logic_vector (5 downto 0);
  signal s_contClkEn : std_logic;
  signal s_SelectAxi : std_logic;
  
  signal s_axiUpdate : std_logic;
  signal s_axiData0 : std_logic_vector(31 downto 0);
  signal s_axiData1 : std_logic_vector(31 downto 0);
 
  signal s_strb : std_logic;
  signal s_data0 : std_logic_vector(C_SREG_WIDTH-1 downto 0);
  signal s_data1 : std_logic_vector(C_SREG_WIDTH-1 downto 0);
  
  
begin

-- Instantiation of Axi Bus Interface S00_AXI
axi_scanOut_v1_0_S00_AXI_inst : axi_scanOut_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
    cntrlReg_o => s_cntrlReg,
    data0_o => s_axiData0,
    data1_o => s_axiData1,
    update_o => s_axiUpdate,
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

	-- Add user logic here
	
	dataCntrl_inst: dataCntrl
  generic map( 
    G_DATA_WIDTH => 24 
    )     
  Port map(     
    clk_i => s00_axi_aclk,
    arstn_i => s00_axi_aresetn,
    cntrlReg_i => s_cntrlReg,
    strb_i => strb_i,
    data0_i => data0_i,
    data1_i => data1_i,
    axiStrb_i => s_axiUpdate,
    axiData0_i => s_axiData0, 
    axiData1_i => s_axiData1,    
    clkDiv_o =>  s_clkDiv,  
    contClkEn_o => s_contClkEn,      
    spiMode_o => s_spiMode, 
    srst_o => s_srst,          
    strb_o => s_strb,           
    data0_o => s_data0,
    data1_o => s_data1
    );
  
  spi_inst:  scanOutSpi 
  generic map(
    G_SREG_WIDTH => C_SREG_WIDTH
    )
  port map( 
    clk_i => s00_axi_aclk,
    srst_i => s_srst,
    strb_i => s_strb,
    clkDiv_i => s_clkDiv,
    contClkEn_i => s_contClkEn,
    mode_i => s_spiMode,        
    data0_i => s_data0,
    data1_i => s_data1,
    ready_o => ready_o,        
    sclk_o => sclk_o,
    sync_o => sync_o,
    sdata0_o => sdata0_o,
    sdata1_o => sdata1_o               
    );
    
  debug_o(6 downto 0) <= s_cntrlReg(6 downto 0);
  debug_o(7) <= s_axiUpdate;
  
	-- User logic ends

end arch_imp;
