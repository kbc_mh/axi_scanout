----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.03.2019 13:36:54
-- Design Name: 
-- Module Name: dataCntrl - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
  use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dataCntrl is
  generic ( 
    G_DATA_WIDTH : natural := 24 );    
  Port ( 
    clk_i : in STD_LOGIC;
    arstn_i : in STD_LOGIC;           
    cntrlReg_i : in std_logic_vector(31 downto 0);
    strb_i : in std_logic;
    data0_i : in std_logic_vector(17 downto 0);
    data1_i : in std_logic_vector(17 downto 0);
    axiStrb_i : in std_logic;
    axiData0_i : in std_logic_vector(31 downto 0); 
    axiData1_i : in std_logic_vector(31 downto 0);    
    clkDiv_o : out std_logic_vector(5 downto 0);         
    contClkEn_o : out std_logic;
    spiMode_o : out std_logic_vector(2 downto 0);
    srst_o : out std_logic;           
    strb_o : out std_logic;           
    data0_o : out std_logic_vector(G_DATA_WIDTH-1 downto 0);
    data1_o : out std_logic_vector(G_DATA_WIDTH-1 downto 0)
    );
end dataCntrl;

architecture Behavioral of dataCntrl is

  component rst_synchronizer is
  generic (
    G_RELEASE_DELAY_CYCLES : natural := 2 -- delay between the de-assertion of the asynchronous and synchronous resets  
    );
  Port ( 
    rst_i : in std_logic;
    clk_i : in std_logic;
    rst_o : out std_logic
    );
  end component rst_synchronizer;
  
  signal s_aRst : std_logic; 
  signal s_swRst : std_logic;
  signal s_sRst : std_logic;

  signal s_mode : std_logic_vector(2 downto 0);
  signal s_spiMode : std_logic_vector(2 downto 0);
  signal s_clkDiv : std_logic_vector (5 downto 0);
  signal s_contClkEn : std_logic;
  signal s_srcAxi : std_logic;
  signal s_strb_r : std_logic;
  signal s_ad5683cmd : std_logic_vector(3 downto 0) := "0011"; -- Write DAC and input register
  signal s_data0 : std_logic_vector(G_DATA_WIDTH-1 downto 0);
  signal s_data1 : std_logic_vector(G_DATA_WIDTH-1 downto 0);  
  signal s_data0_r : std_logic_vector(G_DATA_WIDTH-1 downto 0);
  signal s_data1_r : std_logic_vector(G_DATA_WIDTH-1 downto 0);

  signal s_clkFreq : bit_vector(1 downto 0);
  
begin

  s_clkFreq <= to_bitvector(cntrlReg_i(1 downto 0));

  with s_clkFreq select --Set Clock freq.      
  s_clkDiv <= std_logic_vector(to_unsigned(50,6)) when "00",      
              std_logic_vector(to_unsigned(20,6)) when "01",
              std_logic_vector(to_unsigned(10,6)) when "10",
              std_logic_vector(to_unsigned(4,6))  when "11";

  s_contClkEn <= cntrlReg_i(2);
  
  s_mode <= cntrlReg_i(5 downto 3);  
      
  s_srcAxi <= cntrlReg_i(6);
  
  s_swRst <= cntrlReg_i(31); 

  
	s_aRst <= (not arstn_i) or s_swRst;
	
	resetSync_inst : rst_synchronizer
	generic map(
    G_RELEASE_DELAY_CYCLES => 4  
    )
  Port map( 
    rst_i => s_aRst,
    clk_i => clk_i,
    rst_o => s_srst
    );

buildFrame:
  process(s_mode,data0_i,data1_i)
  begin
    case(s_mode) is
      when "000" => -- XY2-100 16Bit
        s_spiMode <= "000";
        s_data0 <= (G_DATA_WIDTH-1 downto 16 => '0') & data0_i(15 downto 0);
        s_data1 <= (G_DATA_WIDTH-1 downto 16 => '0') & data1_i(15 downto 0);
      when "001" => -- XY2-100 18Bit
        s_spiMode <= "001";
        s_data0 <= (G_DATA_WIDTH-1 downto 18 => '0') & data0_i(17 downto 0);
        s_data1 <= (G_DATA_WIDTH-1 downto 18 => '0') & data1_i(17 downto 0);
      when "010" => -- AD5683      
        s_spiMode <= "010";
        s_data0 <= s_ad5683cmd & data0_i(15 downto 0) & (s_data0'high-20 downto 0 => '0');
        s_data1 <= s_ad5683cmd & data1_i(15 downto 0) & (s_data0'high-20 downto 0 => '0');
      when others =>
        s_spiMode <= "000";
        s_data0 <= (others => '0');
        s_data1 <= (others => '0');
    end case;
  end process;
  
  
axiData:
  process(clk_i)
  begin
    if(rising_edge(clk_i)) then
      if(s_srst = '1') then
        s_strb_r <= '0';        
        s_data0_r <= (others => '0');
        s_data1_r <= (others => '0');
      elsif(s_srcAxi = '1') then
        s_strb_r <= axiStrb_i;        
        s_data0_r <= axiData0_i(G_DATA_WIDTH-1 downto 0);
        s_data1_r <= axiData1_i(G_DATA_WIDTH-1 downto 0);
      else
        s_strb_r <= strb_i;        
        s_data0_r <= s_data0;
        s_data1_r <= s_data1;
      end if;
    end if;
  end process;

  clkDiv_o <= s_clkDiv;
  contClkEn_o <= s_contClkEn;
  spiMode_o <= s_spiMode;
  strb_o <= s_strb_r;
  data0_o <= s_data0_r;
  data1_o <= s_data1_r;
  srst_o <= s_srst;

end Behavioral;
