----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.03.2019 15:14:32
-- Design Name: 
-- Module Name: scanOutSpi - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_MISC.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity scanOutSpi is
    generic ( G_SREG_WIDTH : natural := 24 );     
    port ( clk_i : in STD_LOGIC;
           srst_i : in STD_LOGIC;
           strb_i : in STD_LOGIC;
           contClkEn_i : in STD_LOGIC;
           mode_i : in STD_LOGIC_VECTOR(2 downto 0);
           clkDiv_i : in STD_LOGIC_VECTOR(5 downto 0);
           data0_i : in STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0);
           data1_i : in STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0);
           ready_o : out std_logic;
           sclk_o : out std_logic;
           sync_o : out std_logic;
           sdata0_o : out std_logic;
           sdata1_o : out std_logic);
end scanOutSpi;

architecture Behavioral of scanOutSpi is

  signal s_clkDivCnt_r : unsigned(5 downto 0) := (others => '0');

  type t_shiftState is (idle,shift);
  signal s_shiftState_r : t_shiftState := idle;
  signal s_shiftState_dr : t_shiftState := idle;
  
  type t_clkState is (idle,clocking);
  signal s_clkState_r : t_clkState := idle; 
 
  type t_syncState is (idle,waitForSync,sync);
  signal s_syncState : t_syncState := idle;
  
  signal s_shift0_r : STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0) := (others => '0');
  signal s_shift1_r : STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0) := (others => '0');

  signal s_sync_r : STD_LOGIC := '0'; -- sync signal (routed to out port)

  signal s_strb_r : std_logic := '0';
  signal s_strbReq_r : std_logic := '0';
  
  signal s_contClkEn_r : std_logic := '0'; 
  signal s_sclk_r : std_logic := '0';  --serial clock signal
  signal s_sclk_dr : std_logic := '0'; --serial clock signal (delayed, aligned to sdata) 
  signal s_sclk1st : std_logic; -- 1st edge pulse
  signal s_sclk2nd : std_logic; -- 2nd edge pulse

  signal s_bitCnt_r : unsigned(4 downto 0) := (others => '0');
  
  signal s_nBits : unsigned(4 downto 0);
  signal s_nBits_r : unsigned(4 downto 0) := (others => '0');
  
  signal s_syncPol : std_logic; -- Sync signal polarity (1 when Sync is high during bit shift)
  signal s_syncPol_r : std_logic := '0';
  
  signal s_syncStart : unsigned(4 downto 0);
  signal s_syncStart_r : unsigned(4 downto 0) := (others => '0');
  
  signal s_syncStop : unsigned(4 downto 0);
  signal s_syncStop_r : unsigned(4 downto 0) := (others => '0');

  signal s_sclkPol : std_logic; -- Serial clock polarity (1 when bit cyle starts with rising edge)
  signal s_sclkPol_r : std_logic := '0';

  signal s_ready : std_logic;
  
  -- data frames routed to shift regs
  signal s_outData0 : STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0); 
  signal s_outData0_r : STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0) := (others => '0');
  
  signal s_outData1 : STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0);
  signal s_outData1_r : STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0) := (others => '0');


begin

config: -- prepare data frame according to selected mode
  process(mode_i,data0_i,data1_i)
  variable v_parity0 : STD_LOGIC;
  variable v_parity1 : STD_LOGIC; 
  variable v_parData0 : STD_LOGIC_VECTOR (18 downto 0);
  variable v_parData1 : STD_LOGIC_VECTOR (18 downto 0);   
  begin    
    case mode_i is      
      when "000" => -- XY2-100 16Bit + 3bit command (even paraity)  
        v_parData0 := "001" & data0_i(15 downto 0);
        v_parData1 := "001" & data1_i(15 downto 0);
      when "001" => -- XY2-100 18Bit + 1bit comand (odd parity -> cmd bit zero for parity calculation)
        v_parData0 := "0" & data0_i(17 downto 0);
        v_parData1 := "0" & data1_i(17 downto 0);
      when "010" => -- Analog Devices DACs (24Bit Command + Data) 
        v_parData0 := (others => '0');
        v_parData1 := (others => '0');     
      when others => 
        v_parData0 := (others => '0');
        v_parData1 := (others => '0');     
    end case;
    
    v_parity0 := xor_reduce(v_parData0);
    v_parity1 := xor_reduce(v_parData1);
    
    case mode_i is      
      when "000" => -- XY2-100 16Bit         
        s_outData0 <= "001" & data0_i(15 downto 0) & v_parity0 & (s_outData0'high-v_parData0'length-1 downto 0 => '0');  
        s_outData1 <= "001" & data1_i(15 downto 0) & v_parity1 & (s_outData1'high-v_parData1'length-1 downto 0 => '0');  
        s_nBits <= to_unsigned(20,5); 
        s_sclkPol <= '1';
        s_syncPol <= '0';
        s_syncStart <= to_unsigned(1,5);
        s_syncStop <= to_unsigned(0,5);      
      when "001" => -- XY2-100 18Bit
        s_outData0 <= "1" & data0_i(17 downto 0) & v_parity0 & (s_outData0'high-v_parData0'length-1 downto 0 => '0');   
        s_outData1 <= "1" & data1_i(17 downto 0) & v_parity1 & (s_outData1'high-v_parData1'length-1 downto 0 => '0');  
        s_nBits <= to_unsigned(20,5);
        s_sclkPol <= '1';
        s_syncPol <= '0';
        s_syncStart <= to_unsigned(1,5);
        s_syncStop <= to_unsigned(0,5);
      when "010" => -- Analog Devices DACs
        s_outData0 <= data0_i(23 downto 0);  
        s_outData1 <= data1_i(23 downto 0);       
        s_nBits <= to_unsigned(25,5);  
        s_sclkPol <= '1';   
        s_syncPol <= '0';
        s_syncStart <= to_unsigned(25,5);
        s_syncStop <= to_unsigned(1,5); 
      when others => 
        s_outData0 <= (others => '0');
        s_outData1 <= (others => '0');
        s_nBits <= to_unsigned(20,5);   
        s_sclkPol <= '0'; 
        s_syncPol <= '0';
        s_syncStart <= to_unsigned(1,5);
        s_syncStop <= to_unsigned(0,5); 
    end case;
  end process;

strbReq:
  process(clk_i)
  begin
    if(rising_edge(clk_i)) then
      if(srst_i = '1') then
        s_strbReq_r <= '0';      
      elsif(s_shiftState_r = shift and s_shiftState_dr = idle) then 
        s_strbReq_r <= '0';
      elsif(s_ready = '1' and strb_i = '1') then
        s_strbReq_r <= '1'; 
        s_outData0_r <= s_outData0;
        s_outData1_r <= s_outData1;
        s_nBits_r <= s_nBits;    
        s_sclkPol_r <= s_sclkPol;
        s_syncPol_r <= s_syncPol;
        s_syncStart_r <= s_syncStart;
        s_syncStop_r <= s_syncStop;
      end if; 
    end if;
  end process;
  

sclkGen: -- generate serial clock
  process(clk_i)
  begin
    if(rising_edge(clk_i)) then
     s_strb_r <= '0';
      if(srst_i = '1') then        
        s_clkState_r <= idle;
        s_bitCnt_r <= (others => '0');        
        s_clkDivCnt_r <= (shift_right(unsigned(clkDiv_i),1)-1);
        s_sclk_r <= s_sclkPol_r;       
      else  
        case s_clkState_r is 
        when idle =>
          if(s_strbReq_r = '1') then
            s_strb_r <= '1';           
            s_clkDivCnt_r <= shift_right(unsigned(clkDiv_i),1);
            s_bitCnt_r <= s_nBits_r;                   
            s_sclk_r <= s_sclkPol_r;
            s_clkState_r <= clocking;
          end if;
        when clocking =>                
          if(s_clkDivCnt_r = 1) then                                                      
            s_clkDivCnt_r <= shift_right(unsigned(clkDiv_i),1);
            s_sclk_r <= not(s_sclk_r);                         
                     
            if(s_sclk_r = not(s_sclkPol_r)) then              
              if(s_bitCnt_r > 0) then  
                s_bitCnt_r <= s_bitCnt_r - 1;    
              end if;           
              
              if(s_bitCnt_r < 2) then                                                                                                                                                               
                if(s_strbReq_r = '1') then
                  s_strb_r <= '1';
                  s_bitCnt_r <= s_nBits_r;                                                    
                elsif(contClkEn_i = '0') then
                  s_clkState_r <= idle;
                end if;                                                                                                    
              end if;
              
            end if;
          else
            s_clkDivCnt_r <= s_clkDivCnt_r - 1;                                                                  
          end if;        
        end case;
      end if;
      s_sclk_dr <= s_sclk_r;
    end if;
  end process;

  s_ready <= '1' when s_shiftState_r = idle else 
             '0';

  s_sclk1st <= (s_sclk_r xnor s_sclkPol_r) and (s_sclk_dr xor s_sclkPol_r); -- 1st sclk edge pulse
  s_sclk2nd <= (s_sclk_r xor s_sclkPol_r) and (s_sclk_dr xnor s_sclkPol_r); -- 2nd sclk edge pulse

syncGen: -- generate sync signal
  process(clk_i) 
  begin
    if(rising_edge(clk_i)) then 
      s_sync_r <= not s_syncPol_r;
      if(srst_i = '1') then 
        s_syncState <= idle;        
      else
        case s_syncState is
        when idle =>          
        when waitForSync =>     
          if(s_bitCnt_r = s_syncStart_r) then
            s_sync_r <= s_syncPol_r;              
            s_syncState <= sync;            
          end if;                       
        when sync =>            
          if(s_bitCnt_r = s_syncStop_r) then            
            s_syncState <= idle;   
          else  
            s_sync_r <= s_syncPol_r;     
          end if;
        end case;  
        
        if(s_strbReq_r = '1') then
          if(s_syncStart_r = s_nBits_r) then                          
            s_syncState <= sync; 
          else
            s_syncState <= waitForSync; 
          end if;  
        end if;    
         
      end if;
    end if;
  end process;


shiftOut: -- shift data
  process(clk_i)
  begin            
    if(rising_edge(clk_i)) then      
      if(srst_i = '1') then        
        s_shiftState_r <= idle;  
        s_shiftState_dr <= idle;               
        s_shift0_r <= (others => '0');         
        s_shift1_r <= (others => '0');                                
      else        
        s_shiftState_dr <= s_shiftState_r;
        case s_shiftState_r is          
        when idle =>
          if(s_strb_r = '1') then                                                       
            s_shift0_r <= s_outData0_r;            
            s_shift1_r <= s_outData1_r;          
            s_shiftState_r <= shift;            
          end if;
        when shift =>  
          if(s_sclk1st = '1') then                                                                              
            s_shift0_r <= s_shift0_r(G_SREG_WIDTH-2 downto 0) & '0';                        
            s_shift1_r <= s_shift1_r(G_SREG_WIDTH-2 downto 0) & '0';            
            
            if(s_bitCnt_r = 1) then                                          
              s_shiftState_r <= idle;                       
            end if; 
          end if;                      
        end case;
      end if;
    end if;
  end process;

  sclk_o <= s_sclk_dr;
  sync_o <= s_sync_r;    
  sdata0_o <= s_shift0_r(G_SREG_WIDTH-1);
  sdata1_o <= s_shift1_r(G_SREG_WIDTH-1);
  
  ready_o <= s_ready;

end Behavioral;
