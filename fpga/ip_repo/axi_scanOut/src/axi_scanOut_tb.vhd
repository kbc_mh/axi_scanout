----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.09.2018 15:36:16
-- Design Name: 
-- Module Name: pulseDelay_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity axi_scanOut_tb is
--  Port ( );
end axi_scanOut_tb;

architecture Behavioral of axi_scanOut_tb is
  component dataCntrl is
  generic ( 
    G_DATA_WIDTH : natural := 24 );     
  Port (     
    clk_i : in STD_LOGIC;
    srst_i : in STD_LOGIC;           
    cntrlReg_i : in std_logic_vector(31 downto 0);
    strb_i : in std_logic;
    data0_i : in std_logic_vector(17 downto 0);
    data1_i : in std_logic_vector(17 downto 0);
    axiStrb_i : in std_logic;
    axiData0_i : in std_logic_vector(31 downto 0); 
    axiData1_i : in std_logic_vector(31 downto 0);    
    clkDiv_o : out std_logic_vector(5 downto 0);  
    contClkEn_o : out std_logic;       
    spiMode_o : out std_logic_vector(2 downto 0);           
    strb_o : out std_logic;           
    data0_o : out std_logic_vector(23 downto 0);
    data1_o : out std_logic_vector(23 downto 0)
    );
  end component dataCntrl;
  
  component scanOutSpi is
  generic ( 
    G_SREG_WIDTH : natural := 24 );     
  port ( 
    clk_i : in STD_LOGIC;
    srst_i : in STD_LOGIC;           
    strb_i : in STD_LOGIC;
    contClkEn_i : in STD_LOGIC;
    mode_i : in STD_LOGIC_VECTOR(2 downto 0);
    clkDiv_i : in STD_LOGIC_VECTOR(5 downto 0);
    data0_i : in STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0);
    data1_i : in STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0);
    ready_o : out std_logic;
    sclk_o : out std_logic;
    sync_o : out std_logic;
    sdata0_o : out std_logic;
    sdata1_o : out std_logic 
  );
  end component scanOutSpi;
    
  constant C_SREG_WIDTH : natural := 24;
  
  signal clk_i : std_logic;
  signal srst_i : std_logic;
  signal strb_i : std_logic;
  signal data0_i : std_logic_vector(C_SREG_WIDTH-1 downto 0);
  signal data1_i : std_logic_vector(C_SREG_WIDTH-1 downto 0); 
  
  
  signal clkFreq_i : std_logic_vector (1 downto 0) := "11";
  signal contClkEn_i : std_logic := '0';
  signal mode_i : std_logic_vector(2 downto 0) := "000";
  signal SelectAxi_i : std_logic := '1';

  signal ready_o : std_logic;
  signal sclk_o : std_logic;
  signal sync_o : std_logic;
  signal sdata0_o : std_logic;
  signal sdata1_o : std_logic;
  
  signal s_cntrlReg : std_logic_vector(31 downto 0);
  signal s_spiMode : std_logic_vector(2 downto 0);
  signal s_clkDiv : std_logic_vector (5 downto 0);
  signal s_contClkEn : std_logic;
  signal s_SelectAxi : std_logic;
  
  signal s_axiUpdate : std_logic;
  signal s_axiData0 : std_logic_vector(31 downto 0);
  signal s_axiData1 : std_logic_vector(31 downto 0);
 
  signal s_strb : std_logic;
  signal s_data0 : std_logic_vector(C_SREG_WIDTH-1 downto 0);
  signal s_data1 : std_logic_vector(C_SREG_WIDTH-1 downto 0);    
        
        
        
  constant clk_period : time := 10ns;        

begin

  s_cntrlReg <= x"0000" & '0' & SelectAxi_i & mode_i & contClkEn_i & clkFreq_i;

  uut_dc: dataCntrl
  generic map( 
    G_DATA_WIDTH => 24 
    )     
  Port map(     
    clk_i => clk_i,
    srst_i => srst_i,
    cntrlReg_i => s_cntrlReg,
    strb_i => strb_i,
    data0_i => data0_i,
    data1_i => data1_i,
    axiStrb_i => s_axiUpdate,
    axiData0_i => s_axiData0, 
    axiData1_i => s_axiData1,    
    clkDiv_o =>  s_clkDiv,  
    contClkEn_o => s_contClkEn,      
    spiMode_o => s_spiMode,           
    strb_o => s_strb,           
    data0_o => s_data0,
    data1_o => s_data1
    );

  
  
  uut_spi:  scanOutSpi 
  generic map(
    G_SREG_WIDTH => C_SREG_WIDTH
    )
  port map( 
    clk_i => clk_i,
    srst_i => srst_i,
    strb_i => s_strb,
    clkDiv_i => s_clkDiv,
    contClkEn_i => s_contClkEn,
    mode_i => s_spiMode,        
    data0_i => s_data0,
    data1_i => s_data1,
    ready_o => ready_o,        
    sclk_o => sclk_o,
    sync_o => sync_o,
    sdata0_o => sdata0_o,
    sdata1_o => sdata1_o               
    );
        
    --clk process
    process
    begin
        clk_i <= '1';
        wait for clk_period/2;
        clk_i <= '0';
        wait for clk_period/2;
    end process;    
         

    -- stimulus
    process
    begin
        wait for clk_period*5;
        srst_i <= '1';
        wait for clk_period*10;
        srst_i <= '0';    
        
        wait for 123.5ns;
        
        data0_i <= "1100" & x"AAAA" & "0000";
        data1_i <= "0011" & x"AAAA" & "0000";
        wait for clk_period;   
                             
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        
        wait for clk_period*299;
        data0_i <= "00000001" & "0101010101010101";
        data1_i <= "00000001" & "0101010101010101";
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*299;
        
        mode_i <= "010";
        data0_i <= "00000001" & "0101010101010100";
        data1_i <= "00000001" & "0101010101010100";
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*249;
        
        strb_i <= '1';
        contClkEn_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*249;
        
        mode_i <= "001";
        
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*249;
        
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*249;
        
        data0_i <= "00000001" & "0101010101010101";
        data1_i <= "00000001" & "0101010101010101";
        
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*249;
        
        wait;
    end process;
    
end Behavioral;
