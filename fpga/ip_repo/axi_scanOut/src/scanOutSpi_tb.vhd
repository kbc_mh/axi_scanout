----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.09.2018 15:36:16
-- Design Name: 
-- Module Name: pulseDelay_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity scanOutSpi_tb is
--  Port ( );
end scanOutSpi_tb;

architecture Behavioral of scanOutSpi_tb is
    component scanOutSpi is       
    generic ( G_SREG_WIDTH : natural := 24 );     
    port ( clk_i : in STD_LOGIC;
           srst_i : in STD_LOGIC;
           strb_i : in STD_LOGIC;
           contClkEn_i : in STD_LOGIC;
           mode_i : in STD_LOGIC_VECTOR(2 downto 0);
           clkDiv_i : in STD_LOGIC_VECTOR(5 downto 0);
           data0_i : in STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0);
           data1_i : in STD_LOGIC_VECTOR (G_SREG_WIDTH-1 downto 0);
           sclk_o : out std_logic;
           sync_o : out std_logic;
           sdata0_o : out std_logic;
           sdata1_o : out std_logic);
    end component scanOutSpi;
    
    constant C_SREG_WIDTH : natural := 24;
    
    signal clk_i : std_logic := '0';
    signal srst_i : std_logic := '0';
    signal strb_i : std_logic := '0';
    signal contClkEn_i : std_logic := '0';
    signal mode_i : std_logic_vector(2 downto 0) := std_logic_vector(to_unsigned(2,3));
    signal clkDiv_i : std_logic_vector (5 downto 0) := std_logic_vector(to_unsigned(2,6));
    

    signal data0_i : std_logic_vector (C_SREG_WIDTH-1 downto 0) := std_logic_vector(to_unsigned(10,C_SREG_WIDTH));
    signal data1_i : std_logic_vector (C_SREG_WIDTH-1 downto 0) := std_logic_vector(to_unsigned(10,C_SREG_WIDTH));

    signal sclk_o : STD_LOGIC;
    signal sync_o : STD_LOGIC;
    signal sdata0_o : STD_LOGIC;
    signal sdata1_o : STD_LOGIC;
      
    signal initShift_r : std_logic := '0';        
        
    constant clk_period : time := 10ns;        

begin

    uut:  scanOutSpi 
    port map( 
        clk_i => clk_i,
        srst_i => srst_i,
        strb_i => strb_i,
        clkDiv_i => clkDiv_i,
        contClkEn_i => contClkEn_i,
        mode_i => mode_i,        
        data0_i => data0_i,
        data1_i => data1_i,        
        sclk_o => sclk_o,
        sync_o => sync_o,
        sdata0_o => sdata0_o,
        sdata1_o => sdata1_o               
        );
        
    --clk process
    process
    begin
        clk_i <= '1';
        wait for clk_period/2;
        clk_i <= '0';
        wait for clk_period/2;
    end process;    
         

    -- stimulus
    process
    begin
        wait for clk_period*5;
        srst_i <= '1';
        wait for clk_period*10;
        srst_i <= '0';    
        
        wait for 123.5ns;
        
        data0_i <= "1100" & x"AAAA" & "0000";
        data1_i <= "0011" & x"AAAA" & "0000";
        wait for clk_period;   
                             
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        
        wait for clk_period*299;
        data0_i <= "00000001" & "0101010101010101";
        data1_i <= "00000001" & "0101010101010101";
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*299;
        
        mode_i <= "000";
        data0_i <= "00000001" & "0101010101010100";
        data1_i <= "00000001" & "0101010101010100";
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*249;
        
        strb_i <= '1';
        contClkEn_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*249;
        
        mode_i <= std_logic_vector(to_unsigned(1,3));
        
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*249;
        
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*249;
        
        data0_i <= "00000001" & "0101010101010101";
        data1_i <= "00000001" & "0101010101010101";
        
        strb_i <= '1';
        wait for clk_period;
        strb_i <= '0';
        wait for clk_period*249;
        
        wait;
    end process;
    
end Behavioral;
