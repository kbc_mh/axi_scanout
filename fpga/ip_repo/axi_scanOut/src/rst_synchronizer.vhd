----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.03.2014 11:43:59
-- Design Name: 
-- Module Name: rst_synchronizer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rst_synchronizer is
  generic (
    G_RELEASE_DELAY_CYCLES : natural := 2 -- delay between the de-assertion of the asynchronous and synchronous resets  
    );
  Port ( 
    rst_i : in std_logic;
    clk_i : in std_logic;
    rst_o : out std_logic
    );
begin
  assert G_RELEASE_DELAY_CYCLES >= 2 report "delay must be >= 2 to prevent metastable output" severity failure;
end rst_synchronizer;

architecture Behavioral of rst_synchronizer is

  component synchronizer
      generic (          
        G_INIT_VAL      : std_logic := '0';
        G_NUM_GUARD_FFS : natural := 1
        );    
      port ( 
        clk_i   : in  std_logic;
        rst_i    : in  std_logic;
        async_i : in  std_logic;
        sync_o  : out  std_logic
        );
  end component;

begin

  sync_i : synchronizer
    generic map(
      G_INIT_VAL      => '1',
      G_NUM_GUARD_FFS => G_RELEASE_DELAY_CYCLES - 1)
    port map(
      clk_i   => clk_i,
      rst_i   => rst_i,      
      async_i => '0',
      sync_o  => rst_o);
end Behavioral;
