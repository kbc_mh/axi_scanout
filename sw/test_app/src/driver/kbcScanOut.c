/******************************************************************************
*
* Copyright (C) 2019 Koenig & Bauer Coding GmbH
*
******************************************************************************/
/*****************************************************************************/
/**
* @file kbcScanOut.c
* @{
*
* The implementation of the KBCscanOut low level driver.
*
* @note
*
* None
*
* <pre>
* MODIFICATION HISTORY:
*
* Ver   Who  Date     Changes
* ----- ---- -------- -----------------------------------------------
* 1.00a mh2  12/04/19 First release
* </pre>
*
*****************************************************************************/

/***************************** Include Files *******************************/
#include "kbcScanOut.h"
/************************** Constant Definitions ****************************/

/**************************** Type Definitions ******************************/

/***************** Macros (Inline Functions) Definitions ********************/

/************************** Variable Definitions *****************************/
extern KBCScanOut_Config KBCScanOut_ConfigTable[XPAR_KBCSCANOUT_NUM_INSTANCES];

/************************** Function Prototypes *****************************/


/******************************************************************************/
/**
* Lookup the device configuration based on the unique device ID. The table
* ConfigTable contains the configuration info for each device in the system.
*
* @param	DeviceId is the device identifier to lookup.
*
* @return
*		 - A pointer of data type KBCScanOut_Config which points to the
*		device configuration if DeviceID is found.
* 		- NULL if DeviceID is not found.
*
* @note		None.
*
******************************************************************************/
KBCScanOut_Config *KBCScanOut_LookupConfig(u16 DeviceId)
{
	KBCScanOut_Config *pCfg = NULL;

	int Index;

	for (Index = 0; Index < XPAR_KBCSCANOUT_NUM_INSTANCES; Index++) {
		if (KBCScanOut_ConfigTable[Index].DeviceId == DeviceId) {
			pCfg = &KBCScanOut_ConfigTable[Index];
			break;
		}
	}

	return pCfg;
}

/****************************************************************************/
/**
* Initialize the KBCScanOut instance provided by the caller based on the
* given configuration data.
*
* Nothing is done except to initialize the InstancePtr.
*
* @param	pInst is a pointer to an KBCScanOut instance. The memory the
*		pointer references must be pre-allocated by the caller. Further
*		calls to manipulate the driver through the KBCScanOut API must be
*		made with this pointer.
* @param	pConfig is a reference to a structure containing information
*		about a specific KBCScanOut device. This function initializes an
*		pInst object for a specific device specified by the
*		contents of pConfig. This function can initialize multiple
*		instance objects with the use of multiple calls giving different
*		Config information on each call.
* @param 	EffectiveAddr is the device base address in the virtual memory
*		address space. The caller is responsible for keeping the address
*		mapping from EffectiveAddr to the device physical base address
*		unchanged once this function is invoked. Unexpected errors may
*		occur if the address mapping changes after this function is
*		called. If address translation is not used, use
*		Config->BaseAddress for this parameters, passing the physical
*		address instead.
*
* @return
* 		- XST_SUCCESS if the initialization is successfull.
*
* @note		None.
*
*****************************************************************************/
int KBCScanOut_CfgInitialize(KBCScanOut* pInst, KBCScanOut_Config* pConfig,
			UINTPTR EffectiveAddr)
{
	/* Assert arguments */
	Xil_AssertNonvoid(pInst != NULL);

	/* Set some default values. */
	pInst->BaseAddress = EffectiveAddr;

	/*
	 * Indicate the instance is now ready to use, initialized without error
	 */
	pInst->IsReady = XIL_COMPONENT_IS_READY;
	return (XST_SUCCESS);
}

/****************************************************************************/
/**
* Initialize the KBCScanOut instance provided by the caller based on the
* given DeviceID.
*
* Nothing is done except to initialize the Instance pointer.
*
* @param	pInst is a pointer to an KBCScanOut instance. The memory the
*		pointer references must be pre-allocated by the caller. Further
*		calls to manipulate the instance/driver through the KBCScanOut API
*		must be made with this pointer.
* @param	DeviceId is the unique id of the device controlled by this
* 		KBCScanOut instance. Passing in a device id associates the generic
* 		KBCScanOut instance to a specific device, as chosen by the caller or
*		application developer.
*
* @return
*		- XST_SUCCESS if the initialization was successfull.
* 		- XST_DEVICE_NOT_FOUND  if the device configuration data was not
*		found for a device with the supplied device ID.
*
* @note		None.
*
*****************************************************************************/
int KBCScanOut_Initialize(KBCScanOut* pInst, u16 DeviceId)
{
	KBCScanOut_Config *pConfig;

	/*
	 * Assert arguments
	 */
	Xil_AssertNonvoid(pInst != NULL);

	/*
	 * Lookup configuration data in the device configuration table.
	 * Use this configuration info down below when initializing this
	 * driver.
	 */
	pConfig = KBCScanOut_LookupConfig(DeviceId);
	if (pConfig == (KBCScanOut_Config *) NULL) {
		pInst->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return KBCScanOut_CfgInitialize(pInst, pConfig,
			pConfig->BaseAddress);
}

/****************************************************************************/
/**
* Set the input source of the KBCScanout device
*
* @param	pInst is a pointer to an KBCScanout instance to be worked on.
* @param	enable sets the AXI data registers as data source. In order to
* 			transmit data, 1st the AXI data registers must be written via
* 			KBCScanOut_WriteData(), followed by KBCScanOut_Update()
*
* @return	None.
*
*
*****************************************************************************/
void KBCScanOut_SetInputAXI(KBCScanOut *pInst, u32 Enable)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = KBCScanOut_mReadReg(pInst->BaseAddress, KBCSCANOUT_CR_OFFSET);
	Reg = ((Reg & ~KBCSCANOUT_INMODE_MASK) | ((Enable & 0x1)<<KBCSCANOUT_INMODE_SHIFT));
	KBCScanOut_mWriteReg(pInst->BaseAddress, KBCSCANOUT_CR_OFFSET, Reg);
}

/****************************************************************************/
/**
* Set the output mode of the KBCScanout device
*
* @param	pInst is a pointer to an KBCScanout instance to be worked on.
* @param	mode specifies the interface output configuration. The available
* 			operation modes are listed in the KBCScanOut_OutMode enum.
*
* @return	None.
*
*
*****************************************************************************/
void KBCScanOut_SetOutMode(KBCScanOut *pInst, KBCScanOut_OutMode Mode)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = KBCScanOut_mReadReg(pInst->BaseAddress, KBCSCANOUT_CR_OFFSET);
	Reg = ((Reg & ~KBCSCANOUT_OUTMODE_MASK) | ((Mode & 0x3)<<KBCSCANOUT_OUTMODE_SHIFT));
	KBCScanOut_mWriteReg(pInst->BaseAddress, KBCSCANOUT_CR_OFFSET, Mode);
}

/****************************************************************************/
/**
* Set the serial output clock continuous mode
*
* @param	pInst is a pointer to an KBCScanout instance to be worked on.
* @param	enable sets the state of the continuous clock mode. When enabled
* 			the serial clock continuously toggles, even when no data is
* 			transmitted. Write strobes are aligned to the serial clock phase
* 			inside the interface logic
*
* @return	None.
*
*
*****************************************************************************/
void KBCScanOut_SetSclkContinuous(KBCScanOut *pInst, u32 Enable)
{
	u32 Reg;

	/* Assert validates the input arguments */
		Xil_AssertNonvoid(pInst != NULL);
		Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

		Reg = KBCScanOut_mReadReg(pInst->BaseAddress, KBCSCANOUT_CR_OFFSET);
		Reg = ((Reg & ~KBCSCANOUT_CONTCLK_MASK) | ((Enable & 0x1)<<KBCSCANOUT_CONTCLK_SHIFT));
		KBCScanOut_mWriteReg(pInst->BaseAddress, KBCSCANOUT_CR_OFFSET, Reg);
}


/****************************************************************************/
/**
* Set the frequency of the serial clock
*
* @param	pInst is a pointer to an KBCScanout instance to be worked on.
* @param	freq specifies the serial clock frequency. The available
* 			frequency settigns are listed in the KBCScanOut_SclkFreq enum.
*
* @return	None.
*
*
*****************************************************************************/
void KBCScanOut_SetSclkFreq(KBCScanOut *pInst, KBCScanOut_SclkFreq Freq)
{
	u32 Reg;

	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	Reg = KBCScanOut_mReadReg(pInst->BaseAddress, KBCSCANOUT_CR_OFFSET);
	Reg = ((Reg & ~KBCSCANOUT_CLKFREQ_MASK) | ((Freq & 0x3)<<KBCSCANOUT_CLKFREQ_SHIFT));
	KBCScanOut_mWriteReg(pInst->BaseAddress, KBCSCANOUT_CR_OFFSET, Reg);
}

/****************************************************************************/
/**
* Start transmission of a data packet
*
* When the data source of the interface is set to AXI, calling this function
* triggers the transmission the data that is held in the data registers
*
* @param	pInst is a pointer to an KBCScanout instance to be worked on.
*
* @return	None.
*
*
*****************************************************************************/
void KBCScanOut_Update(KBCScanOut *pInst)
{
	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	KBCScanOut_mWriteReg(pInst->BaseAddress, KBCSCANOUT_UR_OFFSET, KBCSCANOUT_UPDATE_MASK);
	KBCScanOut_mWriteReg(pInst->BaseAddress, KBCSCANOUT_UR_OFFSET, ~KBCSCANOUT_UPDATE_MASK);
}

/****************************************************************************/
/**
* Write data to one of the two AXI data registers.
*
* @param	pInst is a pointer to an KBCScanout instance to be worked on.
* @param	Ch specifies the channel to be addressed
* @param	Data is the raw data which is send to the DACs via the serial
* 			interface. For XY2-100 modes data is simply the coordinate to be
* 			addressed. For dedicated DAC modes data must contain a complete
* 			frame including configuration or address settings with correct
* 			bit alignment.
*
* @return	None.
*
*
*****************************************************************************/
void KBCScanOut_WriteData(KBCScanOut *pInst, u32 Ch, u32 Data)
{
	/* Assert validates the input arguments */
	Xil_AssertNonvoid(pInst != NULL);
	Xil_AssertNonvoid(pInst->IsReady == XIL_COMPONENT_IS_READY);

	KBCScanOut_mWriteReg(pInst->BaseAddress, KBCSCANOUT_DATA0_OFFSET+Ch*4, Data);
}
