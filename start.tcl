
# set project name
set project_name axi_scanOut

# check vivado version
set vivado_version [version -short]
puts $vivado_version

# change to project_root_dir (nothing to do, scripts are root dir)
# cd ../../

# set project root dir
set project_root_dir [pwd]

# change to new working dir
cd $project_root_dir/work/vivado/
# save actual working directory
set work_dir [pwd]

# check if project already exists
# if yes, just open
if {[file isfile $project_name.xpr]} {
   puts "----------------------------"
   puts "--                        --"
   puts "-- Open existing project! --"
   puts "--                        --"
   puts "----------------------------"
   
   # open project
   open_project $project_name.xpr

   # switch from -mode batch to -mode gui
# if not, regenerate design
} else {
   puts "--------------------------"
   puts "--                      --"
   puts "-- Re-generate project! --"
   puts "--                      --"
   puts "--------------------------"

   # create new project   
   source $project_root_dir/proj_$vivado_version.tcl
   # create block design process (new approach from Vivado 2018.1)
   source $project_root_dir/bd_$vivado_version.tcl
   
   # create actual processing system block design
   set design_name system
   create_bd_design -dir $project_root_dir/fpga/bd/. $design_name 
   create_root_design ""
}

# start vivado gui
start_gui


